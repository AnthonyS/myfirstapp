import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';

/*
  Generated class for the BackProductsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class BackProductsProvider {
  headers = new Headers()
  // baseUrl: string
  baseUrl = "/api"

  constructor(public http: Http) {
    // this.baseUrl = "/api";
    // this.baseUrl = "https://cors-anywhere.herokuapp.comhttp://fast-badlands-48562.herokuapp.com/api/1.0";
    // this.baseUrl = "http://fast-badlands-48562.herokuapp.com/api/1.0";

    console.log('Hello BackProductsProvider Provider');
  }

  getProducts() {
    return this.http.get(this.baseUrl+'/products', { headers: this.headers }).map(response => response.json()) 
  }

  getCategories() {
    return this.http.get(this.baseUrl+'/categories', { headers: this.headers }).map(response => response.json()) 
  }

  getProductsByCategories(id) {
    return this.http.get(this.baseUrl+'/products/categories/'+id, { headers: this.headers }).map(response => response.json()) 
  }
}
