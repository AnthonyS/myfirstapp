import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { BackProductsProvider } from '../../providers/back-products/back-products';

/**
 * Generated class for the MyProductsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-my-products',
  templateUrl: 'my-products.html',
})
export class MyProductsPage {

  firstname: string
  lastname: string
  products: [any]
  categories: [any]

  constructor(public navCtrl: NavController, public navParams: NavParams, public BackProducts: BackProductsProvider ) {
    this.firstname = this.navParams.get('firstname')
    this.lastname = this.navParams.get('lastname')
  }

  ionViewDidLoad() {
    this.BackProducts.getProducts().subscribe(products => {
      this.products = products
      })
    this.BackProducts.getCategories().subscribe(categories => {
      this.categories = categories
    })
    console.log('ionViewDidLoad MyProductsPage');
  }

  openSelect(event) {
    console.log('On passe par le select');
    this.BackProducts.getProductsByCategories(event.value).subscribe(products => {
      this.products = products
    })
  }

}
