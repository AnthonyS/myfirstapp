import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { MyProductsPage } from '../my-products/my-products';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController, public alertCtrl: AlertController) {
  }

  presentPrompt() {
  const prompt = this.alertCtrl.create({
    title: 'Qui êtes vous ?',
    inputs: [
      {
       placeholder:"firstname",
       name:"firstname",
      },
      {
        placeholder:"lastname",
        name:"lastname",
      },
    ],
    buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        handler: data => {
          console.log('Cancel clicked');
        }
      },

      {
      text: 'Valider',
      handler: data => {
        this.navCtrl.push(MyProductsPage,{
          firstname : data.firstname,
          lastname : data.lastname
        })
      }

    }
    ]
  });
  prompt.present();
}

  goToProducts():void{                  //Donne l'autorisation à la route
    console.log("test");
    this.navCtrl.push(MyProductsPage);
  }
}
