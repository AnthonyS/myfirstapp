import { NgModule } from '@angular/core';
import { AllLetterUpperCasePipe } from './all-letter-upper-case/all-letter-upper-case';
@NgModule({
	declarations: [AllLetterUpperCasePipe],
	imports: [],
	exports: [AllLetterUpperCasePipe]
})
export class PipesModule {}
